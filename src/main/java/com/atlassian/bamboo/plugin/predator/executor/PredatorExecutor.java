package com.atlassian.bamboo.plugin.predator.executor;


import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plugin.predator.PredatorConstants;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jezhumble.javasysmon.JavaSysMon;
import com.jezhumble.javasysmon.ProcessInfo;
import org.apache.commons.io.FileSystemUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.EmptyFileFilter;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tools.ant.taskdefs.condition.Os;
import org.jetbrains.annotations.NotNull;

import javax.resource.NotSupportedException;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PredatorExecutor {

    private static final Logger log = Logger.getLogger(PredatorExecutor.class);
    private static final long COMMAND_TIMEOUT_SECONDS = 120;

    private BuildLogger buildLogger;


    public PredatorExecutor(BuildLogger buildLogger) {
        this.buildLogger = buildLogger;
    }

    public float retrieveFreeSpaceGB(){
        try {
            final long freeSpaceKb = FileSystemUtils.freeSpaceKb();
            return new BigDecimal(freeSpaceKb).divide(new BigDecimal(1024 * 1024)).floatValue();
        } catch (IOException e) {
            logError("Exception when trying to calculate free disk space, using maximum", e);
            // we should not fail on this
            return Float.MAX_VALUE;
        }
    }

    public void removeMavenStable(final boolean deleteStableMaven, @NotNull final File mavenLocalRepoDirectory,
                                  @NotNull final Float ageStableToDelete, @NotNull final Float minimumFreeDisk) throws IOException {
        if (!deleteStableMaven) {
            logOutput("Skipping the removal of maven stable artifacts");
            return;
        }

        if (!mavenLocalRepoDirectory.isDirectory()) {
            logOutput("Maven local repository " + mavenLocalRepoDirectory.getAbsolutePath() + "doesn't exist, skipping the removal of maven snapshots");
            return;
        }

        final long cutoff = (long) (System.currentTimeMillis() - (24 * 60 * 60 * 1000 * ageStableToDelete));
        final Iterator<File> allFiles =  FileUtils.iterateFiles(mavenLocalRepoDirectory, new AgeFileFilter(cutoff), TrueFileFilter.INSTANCE);


        float initialFreeDiskGB = retrieveFreeSpaceGB();
        int countDeleted = 0;
        float totalFreedDiskGB = 0;
        while (allFiles.hasNext() && (initialFreeDiskGB + totalFreedDiskGB < minimumFreeDisk) ){
            File file = allFiles.next();
            totalFreedDiskGB += FileUtils.sizeOf(file) * 1f/ FileUtils.ONE_GB;
            FileUtils.deleteQuietly(file);
            countDeleted++;
        }

        logOutput("Maven stable disk freed: " + totalFreedDiskGB + " GB, deleted " + countDeleted + " files");
    }

    public void removeMavenSnapshots(final boolean deleteSnapshotsMaven, @NotNull final File mavenLocalRepoDirectory) throws IOException {
        if (!deleteSnapshotsMaven) {
            logOutput("Skipping the removal of maven snapshots");
            return;
        }

        if (!mavenLocalRepoDirectory.isDirectory()) {
            logOutput("Maven local repository " + mavenLocalRepoDirectory.getAbsolutePath() + "doesn't exist, skipping the removal of maven snapshots");
            return;
        }

        final Collection<File> allFolders =  FileUtils.listFilesAndDirs(mavenLocalRepoDirectory, FalseFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        List<File> snapshotFolders = Lists.newArrayList(Iterables.filter(allFolders, (File input) -> input.getName().endsWith("-SNAPSHOT")));

        logOutput("Deleting " + snapshotFolders.size() + " snapshot folders from " + mavenLocalRepoDirectory);

        long totalDisk = 0;
        for (File snapshotFolder : snapshotFolders){
            totalDisk += FileUtils.sizeOfDirectory(snapshotFolder);
            FileUtils.deleteQuietly(snapshotFolder);
        }
        logOutput("Maven snapshot disk freed: " + totalDisk / FileUtils.ONE_GB + " GB");
    }

    public void removeEmptyArtifacts(@NotNull final File mavenLocalRepoDirectory) {
        if(!mavenLocalRepoDirectory.isDirectory())
        {
            logOutput("Maven local repository " + mavenLocalRepoDirectory.getAbsolutePath() + "doesn't exist, skipping the removal of empty artifacts");
            return;
        }

        // find all the artifacts that are empty
        final Collection<File> emptyArtifacts =  FileUtils.listFiles(mavenLocalRepoDirectory, EmptyFileFilter.EMPTY, TrueFileFilter.INSTANCE);
        List<File> emptyArtifactList = new ArrayList<>(emptyArtifacts);

        for (File file : emptyArtifactList)
        {
            FileUtils.deleteQuietly(file);
        }

        logOutput(emptyArtifactList.size() + " empty artifacts were found and deleted by Predator");
    }

    public void removeOldBuildWorkingDirectory(final boolean removeBuildWorkingDir, final boolean keepBuildWorkingDirActiveBranches, @NotNull final File agentWorkingDir, @NotNull final String jobKey, final String[] activePlanKeys) {
        removeOldBuildWorkingDirectory(removeBuildWorkingDir, keepBuildWorkingDirActiveBranches, agentWorkingDir, jobKey, activePlanKeys, Executors.newSingleThreadExecutor());
    }

    @VisibleForTesting
    void removeOldBuildWorkingDirectory(final boolean removeBuildWorkingDir, final boolean keepBuildWorkingDirActiveBranches, @NotNull final File agentWorkingDir, @NotNull final String jobKey, final String[] activePlanKeys, @NotNull final ExecutorService deleteService) {
        if (!removeBuildWorkingDir) {
            logOutput("Skipping the removal of build working directory");
            return;
        }
        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            try {
                final Path tempDir = Files.createTempDirectory("predator", new FileAttribute[0]);

                for (File folder : agentWorkingDir.listFiles()) {
                    final String folderName = folder.getName();
                    if (!folderName.equals(jobKey) && !PredatorConstants.BUILD_FOLDERS.contains(folderName)){
                        if (keepBuildWorkingDirActiveBranches && !isActiveBranch(folderName, activePlanKeys)) {
                            logOutput("Skip deleting active branch in working directory " + folderName);
                        } else {
                            File cleanupFolder = new File(tempDir.toFile(), folder.getName() + "-cleanup");
                            try {
                                FileUtils.moveDirectory(folder, cleanupFolder);
                            } catch (IOException e) {
                                logError("Failed to move old build directory prior to deletion.", e);
                            }
                        }
                    }
                }

                deleteService.submit(() -> {
                    logOutput("Deleting old build directory \"" + tempDir.toString() + "\" on a background thread.");
                    FileUtils.deleteQuietly(tempDir.toFile());
                });

                deleteService.shutdown();

            } catch (IOException e) {
                logError("Failed to create a temporary directory", e);
            }
        } else {
            for (File folder : agentWorkingDir.listFiles()) {
                final String folderName = folder.getName();
                if (!folderName.equals(jobKey) && !PredatorConstants.BUILD_FOLDERS.contains(folderName)) {
                    if (keepBuildWorkingDirActiveBranches && isActiveBranch(folderName, activePlanKeys)) {
                        logOutput("Skip deleting active branch in working directory " + folderName);
                    } else {
                        logOutput("Deleting folder " + folder.getAbsolutePath());
                        FileUtils.deleteQuietly(folder);
                    }
                }
            }
        }
    }

    boolean isActiveBranch(String folderName, final String[] activePlanKeys) {
        PlanKey pk;
        try {
            pk = PlanKeys.getPlanKey(folderName);
        } catch (IllegalArgumentException iae) {
            return false;
        }
        String planKey = PlanKeys.isJobKey(pk) ? PlanKeys.getChainKeyFromJobKey(pk).getKey() : pk.getKey();
        return Arrays.asList(activePlanKeys).contains(planKey);
    }

    void killProcesses(@NotNull final String processesToKillRegex) throws IOException, InterruptedException, NotSupportedException {
        // This is dirty, but windows machines can spit up all sort of DLLs problems when loading this library
        // in order to avoid it, let's just rethrow a nicer exception
        final String message = "The current OS does not support process management. Common causes: missing DLLs (msvcr100.dll) in Windows machines, running 32 bits OS";
        try {
            killProcesses(processesToKillRegex, new JavaSysMon());
        } catch (UnsatisfiedLinkError | NoClassDefFoundError unsatisfiedLinkError){
            throw new NotSupportedException(message, unsatisfiedLinkError);
        }
    }

    void killProcesses(@NotNull final String processesToKillRegex, @NotNull JavaSysMon javaSysMon) throws IOException, InterruptedException, NotSupportedException {
        if (StringUtils.isEmpty(processesToKillRegex)){
            logOutput("Skipping processes killing ");
            return;
        }

        if (!javaSysMon.supportedPlatform()) {
            logError("Platform not supported. It's not possible to kill processes.");
            throw new NotSupportedException();
        }

        final List<ProcessInfo> allProcesses = Arrays.asList(javaSysMon.processTable());
        final Map<Integer,ProcessInfo> mappedProcesses = Maps.uniqueIndex(allProcesses, (ProcessInfo from) -> from.getPid());

        // retrieves all the PIDs of the current process branch
        final List<Integer> currentPidTree = Lists.newArrayList();
        final ProcessInfo currentProcess = mappedProcesses.get(javaSysMon.currentPid());
        ProcessInfo process = currentProcess;
        do {
            currentPidTree.add(process.getPid());
            logOutput("Identifying Bamboo agent process (or parent): " + process.toString());
            process = mappedProcesses.get(process.getParentPid());
        } while (process != null && process.getPid() != 0 && !currentPidTree.contains(process.getPid()));
        /* For some reason, windows can have circular dependencies in same cases. I wonder how. */


        final Pattern patternRegex = Pattern.compile(processesToKillRegex, Pattern.CASE_INSENSITIVE);
        final List<ProcessInfo> matchedProcesses = Lists.newArrayList(Iterables.filter(allProcesses,
                        new Predicate<ProcessInfo>() {
                            @Override
                            public boolean apply(ProcessInfo processInfo) {
                                try {
                                    final Integer pid = processInfo.getPid();

                                    boolean toRet = pid != 0 && pid != 1  /* don't kill base processes */
                                            && processInfo.getOwner().equals(currentProcess.getOwner())   /* only for same user */
                                            && !currentPidTree.contains(pid)      /* do not kill the current process */
                                            && !StringUtils.isBlank(processInfo.getCommand())
                                            && patternRegex.matcher(processInfo.getCommand()).find();
                                    if (toRet && hackIsZombie(processInfo)) {
                                        logOutput(String.format("Skipping zombie process PID: %s\t Name: %s\t Owner: %s \t Command: %s",
                                            processInfo.getPid(), processInfo.getName(), processInfo.getOwner(), processInfo.getCommand() ));
                                        return false;
                                    }
                                    return toRet;
                                } catch (NullPointerException e){
                                    /* Apparently due to some security policy or other weirdness, the PID of ssh processes give NPE */
                                    logError("Error retrieving PID of process " + processInfo.getName() + ". It is probably due to system permissions (e.g., a SSH command from other user). Ignoring this process. ");
                                    return false;
                                }
                            }
                        })
        );

        if (matchedProcesses.isEmpty()){
            logOutput("No processes found for " + processesToKillRegex);
            return;
        }

        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (ProcessInfo processInfo : matchedProcesses) {
            executor.submit(() -> {
                logOutput(String.format("Killing process PID: %s\t Name: %s\t Owner: %s \t Command: %s",
                        processInfo.getPid(), processInfo.getName(), processInfo.getOwner(), processInfo.getCommand()));
                javaSysMon.killProcessTree(processInfo.getPid(), false);
            });
        }

        try {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS); // Killing will take no longer than 120 seconds as per logs
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            logOutput("Waited for for 1 hour jobs in process kills pool to complete");
        }
        finally {
            executor.shutdownNow();
            logOutput("Shutdown of process kill pool finished");
        }
    }


    public void runCommands(@NotNull final String[] commands, final boolean runningInBambooDockerContainer)
            throws Exception {
        runCommandWaiters(Arrays.asList(commands).stream()
                    .map((String t) -> t.trim())
                    .filter((String t) -> !t.isEmpty())
                    .map((String t) -> {
                        ProcessBuilder pb = new ProcessBuilder(StringUtils.split(t, " "));
                        pb.environment().put(PredatorConstants.ENV_VAR_NAME_DOCKER_PIPELINE,
                                Boolean.toString(runningInBambooDockerContainer));
                        return new ProcessWaiter(pb);
                    })
                    .collect(Collectors.toList()),
                COMMAND_TIMEOUT_SECONDS);
    }

    void runCommandWaiters(@NotNull List<ProcessWaiter> waiters, long timeoutSeconds) throws Exception {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        for (ProcessWaiter waiter : waiters) {
            logOutput(String.format("Running command '%s'", waiter.command()));
            try {
                Future<Integer> future = executor.submit(waiter);
                Integer exitCode = future.get(timeoutSeconds, TimeUnit.SECONDS);
                logOutput(String.format("Command '%s' completed with exit code %d", waiter.command(), exitCode.intValue()));
            } catch (TimeoutException te) {
                logWarning(String.format("Command '%s' skipped. Took too long to run, prematurely terminated.",
                        waiter.command()));
            } catch (InterruptedException | ExecutionException e) {
                logWarning(String.format("Command '%s' skipped, continuing. This is almost never a cause of failed builds. Exception: \n%s",
                        waiter.command(), e.getMessage()));
            } finally {
                waiter.destroy();
            }
        }

        executor.shutdown();
    }

    public void deleteFiles(@NotNull final String[] files, @NotNull File homeDirectory, boolean force){
        if (files.length == 0){
            logOutput("Skipping selected files deletion ");
            return;
        }

       for (final String fileName : files){
           if (!fileName.isEmpty()) {
               final File file = new File(fileName.replaceFirst("^~", homeDirectory.getAbsolutePath()).trim());
               if (force || file.exists()) {
                   logOutput("Deleting file " + file.getAbsolutePath());
                   FileUtils.deleteQuietly(file);
               } else {
                   logOutput("File " + file.getAbsolutePath() + " does not exist.");
               }
           }
       }
    }

    public boolean shouldRunInThisAgent(@NotNull final String name, @NotNull final String agentsRegex) {
        return name.matches("(?i)" + agentsRegex);
    }


    void logError(String message)
    {
        this.buildLogger.addErrorLogEntry(PredatorConstants.LOG_PREFIX + message);
        log.error(PredatorConstants.LOG_PREFIX + message);
    }

    void logError(String message, Exception e)
    {
        this.buildLogger.addErrorLogEntry(PredatorConstants.LOG_PREFIX + message, e);
        log.error(PredatorConstants.LOG_PREFIX + message, e);
    }

    void logWarning(String message) {
        this.buildLogger.addBuildLogEntry(PredatorConstants.LOG_PREFIX + message);
        log.warn(PredatorConstants.LOG_PREFIX + message);
    }

    public void logOutput(String message)
    {
        this.buildLogger.addBuildLogEntry(PredatorConstants.LOG_PREFIX + message);
        log.info(PredatorConstants.LOG_PREFIX + message);
    }

    public void setBuildLogger(BuildLogger buildLogger) {
        this.buildLogger = buildLogger;
    }

    private boolean hackIsZombie(ProcessInfo processInfo) {
        File proc = new File("/proc/" + processInfo.getPid() +  "/status");
        //exists on linux
        if (proc.exists() && proc.isFile()) {
            try (Stream<String> lines = Files.lines(proc.toPath())) { //close() needs to be called.
                return lines.filter((String t) -> t.contains("State") && t.contains("zombie")).findFirst().isPresent();
            } catch (IOException ex) {
                log.warn("Cannot read " + proc,  ex);
            }
        }
        return false;
    }

}
